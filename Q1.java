
import java.util.ArrayList;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		String word1, sentence1;
		ArrayList sentence2 = new ArrayList();
	    Scanner sc = new Scanner(System.in);
	    Scanner sc2 = new Scanner(System.in);
	    System.out.println("請輸入一個字串進行反轉:");
	    word1 = sc.next();
	    
	    System.out.println("請輸入一行字串進行反轉");
	    sentence1 = sc2.nextLine();
	    sentence2 = reverse_sentence(sentence1);
	    
	    
	    
	    System.out.println("輸出1為 : ");
	    System.out.println(reverse_word(word1));
	    System.out.println("輸出2為 : ");
	    reverse_sentence(sentence1).forEach((a) -> System.out.print(a + " "));
	    
	    }
	
	public static StringBuffer reverse_word(String word1) {

		StringBuffer reverse_1 = new StringBuffer(word1);
		reverse_1.reverse();
		return reverse_1;
		
	}
	
	public static ArrayList reverse_sentence(String sentence1) {
		String[] array = sentence1.split(" ");
		ArrayList after_array = new ArrayList();
		for (int i = 0; i < array.length; i++) {
			StringBuffer split_word = new StringBuffer(array[i]);
			split_word.reverse();
			after_array.add(split_word);
		}
		
		return after_array;
	
	}

}
